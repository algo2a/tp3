#ifndef TP3_FONCTION_H
#define TP3_FONCTION_H

//Déclaration des structures
struct Maillon {
    char *nom;
    Maillon *suivant;
};

struct FileAttente {
    Maillon *tete;
    Maillon *queue;
    int longueur;
};


//Déclarartion des fonction et procédure utilitaires
FileAttente *initialisationListeAttente();

Maillon *initialisationMaillon();

void tabAssign(char *, char *);


//Déclaration des procédure du programme
void menu();

void ajouterPersQueue(char *, FileAttente *);

void retirerPersTete(FileAttente *);

void consulterPersTete(FileAttente *);

void calculerLongueurFile(FileAttente *);

#endif