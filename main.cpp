#include <iostream>
#include <limits>
#include <iomanip>
#include "fonctions.h"

using namespace std;

int main() {

    //Déclaration des constantes
    int taille_input = sizeof(char) * 30;

    //Déclaration des variables
    int choixMain;
    FileAttente *file = initialisationListeAttente();
    char *nom;

    do {
        //Affichage du menu
        menu();

        //Selection du choix par l'utilisateur
        cout << "Entrer votre choix: ";
        cin >> choixMain;
        cout << endl;

        //Test si saisie de l'utilisateur valide
        if (cin.fail()) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "Vous devez entrer un entier ! " << endl;
        } else {

            //Gestion des différents choix possibles
            switch (choixMain) {

                case 1:
                    nom = (char *) malloc(static_cast<size_t>(taille_input));

                    cout << "Veuillez saisir le nom de la personne dans la file d'attente (30 caracteres max): ";
                    cin >> setw(taille_input) >> nom;

                    //Appel de la fonction pour ajouter une personne en queue de la file
                    ajouterPersQueue(nom, file);

                    //Libération de la mémoire
                    nom = nullptr;
                    free(nom);
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    break;

                case 2:
                    //Appel de la fonction pour retirer la personne en tete de la file
                    retirerPersTete(file);
                    break;

                case 3:
                    //Appel de la fonction pour consulter la personne en tete de la file
                    consulterPersTete(file);
                    break;

                case 4:
                    //Appel de la fonction pour afficher le longueur de la file
                    calculerLongueurFile(file);
                    break;

                case 5:
                    cout << "Au revoir !" << endl;
                    break;

                default:
                    cout << "Erreur de saisie !" << endl << "Entrer un choix compris entre 1 et 5 : ";
                    cin >> choixMain;
                    cout << endl;
                    break;
            }
        }
    } while (choixMain != 5);

    //Libération de la mémoire
    free(file);

    return 0;
}