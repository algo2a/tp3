#include <iostream>
#include "fonctions.h"

using namespace std;


////////////////////////
/*
 * Cette fonction permet d'initialiser une nouvelle liste d'attente.
 *
 *  ENTREE : RIEN
 *  SORTIE : Pointeur vers une structure FileAttente
 */
////////////////////////
FileAttente *initialisationListeAttente() {
    FileAttente *file = (FileAttente *) malloc(sizeof(FileAttente));
    file->queue = nullptr;
    file->tete = nullptr;
    file->longueur = 0;
    return file;
}

////////////////////////
/*
 * Cette fonction permet d'initialiser un nouveau maillon.
 *
 *  ENTREE : RIEN
 *  SORTIE : Pointeur vers une structure Maillon
 */
////////////////////////
Maillon *initialisationMaillon(char *nom) {
    Maillon *maillon = (Maillon *) malloc(sizeof(Maillon));
    maillon->nom = nom;
    maillon->suivant = nullptr;
    return maillon;
}


////////////////////////
/*
 * Cette procédure permet d'afficher le menu de selection a l'utilisteur
 *
 *  ENTREE : RIEN
 *  SORTIE : RIEN
 */
////////////////////////
void menu() {
    cout << endl << "//////////////////////// FILE D'ATTENTE ////////////////////" << endl;
    cout << "1 - Ajouter une personne en queue !" << endl;
    cout << "2 - Retirer une personne en tete !" << endl;
    cout << "3 - Consulter la personne en tete de file !" << endl;
    cout << "4- Calculer la longueur de la file d'attente !" << endl;
    cout << "5 - Quitter !" << endl;
}


////////////////////////
/*
 * Cette procédure permet d'ajouter une personne en queue de la file d'attente
 *
 *  ENTREE : Pointeur vers le premier caractères de la chaine de caratère composant nom de la nouvelle personne,
 *              Pointeur vers la file d'attente actuelle
 *  SORTIE : RIEN
 */
////////////////////////
void ajouterPersQueue(char *nom, FileAttente *file) {

    if (nom == nullptr || file == nullptr) {
        cout << "Erreur, impossible d'ajouter une personne a la file d'attente : " << endl;
    } else {

        //Déclaration et initialisation d'un nouveau maillon
        Maillon *newMaillon = initialisationMaillon(nom);

        //Si la liste est vide
        if (file->tete == nullptr) {
            file->tete = newMaillon;
            file->queue = newMaillon;
        } else {
            file->queue->suivant = newMaillon;
            file->queue = newMaillon;
        }

        //Incrémentation de la longueur de la file
        file->longueur = file->longueur + 1;

        cout << "La persone ";
        //Affichage des 30 premiers caractères de la chaine de caractères
        int i = 0;
        while (i < 30 && nom[i] != '\0') {
            cout << nom[i];
            i++;
        }
        cout << " a bien ete ajoute a la file d'attente" << endl;
    }
}


////////////////////////
/*
 * Cette procédure permet de retirer la personne en tete de la file d'attente
 *
 *  ENTREE : Pointeur vers la file d'attente actuelle
 *  SORTIE : RIEN
 */
////////////////////////
void retirerPersTete(FileAttente *file) {

    //Test si la file d'attente existe et n'est pas vide
    if (file != nullptr && file->tete != nullptr) {

        //Déclaration et initialisation d'un pointeur vers caractère afin de garder le nom de la personne supprimé
        char *nomPersSupr = file->tete->nom;

        //Décrémentation de la longueur de la file d'attente
        file->longueur = file->longueur - 1;

        cout << "La personne en tete qui possede le nom " << nomPersSupr << " a bien ete retirer de la file d'attente !"
             << endl;

        //Pointer le tete de la liste vers l'ancien 2ieme maillon de la file d'attente
        file->tete->nom = nullptr;
        file->tete = file->tete->suivant;

    } else {
        cout << "Il n'y a plus personnes dans la file d'attente !" << endl;
    }
}


////////////////////////
/*
 * Cette procédure permet de consulter le nom de la personne en tete de la file d'attente
 *
 *  ENTREE : Pointeur vers la file d'attente actuelle
 *  SORTIE : RIEN
 */
////////////////////////
void consulterPersTete(FileAttente *file) {

    //Si la file existe et n'est pas vide
    if (file != nullptr && file->tete != nullptr) {
        cout << "Le nom de la personne qui est en tete de la file d'attente est : " << file->tete->nom;
    } else {
        cout << "Il n'y a plus personne dans la fille d'attente !";
    }
}


////////////////////////
/*
 * Cette procédure permet de consulter la longueur de la file d'attente
 *
 *  ENTREE : Pointeur vers la file d'attente actuelle
 *  SORTIE : RIEN
 */
////////////////////////
void calculerLongueurFile(FileAttente *file) {

    //Si la file existe et n'est pas vide
    if (file != nullptr && file->tete != nullptr) {
        cout << "Longueur file attente : " << file->longueur << endl;
    } else {
        cout << "La liste est vide" << endl;
    }
}